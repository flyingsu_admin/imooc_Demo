package com.imooc.demo.controller;


import com.imooc.demo.model.RestResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommonControllerTest {

    @Autowired
    private CommonController commonController;

    @Test
    public void helloTest() {
        RestResult<String> restResult = commonController.hello();
        System.out.println(restResult);
    }

//    @Test
//    public void hiTest() {
//        RestResult<String> restResult = commonController.hi();
//        System.out.println(restResult);
//    }
}
