package com.imooc.demo.model;

public enum ResultCode {
    /****************************0开头，系统相关*************************/
    SUCCESS("000000", "SUCCESS"),
    PARAM_ERROR("000001", "Parameter Error!"),
    RECORD_REPEATE("000002", "Record Repeate!"),
    INTERNAL_ERROR("000003", "Internal Error!");
    private String code;
    private String message;

    ResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ResultCode{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
