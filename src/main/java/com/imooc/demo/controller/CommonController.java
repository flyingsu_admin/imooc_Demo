package com.imooc.demo.controller;

import com.imooc.demo.model.RestResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/common")
public class CommonController {

    @GetMapping("/hello")
    public RestResult<String> hello() {
        return new RestResult<>("hello");
    }

//    @GetMapping("/hi")
//    public RestResult<String> hi() {
//        return new RestResult<>("hi");
//    }
}
